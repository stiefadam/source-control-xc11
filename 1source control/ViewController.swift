//
//  ViewController.swift
//  1source control
//
//  Created by Adam Stiefel on 3/30/20.
//  Copyright © 2020 Adam Stiefel. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /**
     Adds two numbers together and returns the result. Modified.
     - parameter num1: The first number.
     - parameter num2: The second number.
     - returns: The sum of num1 and num2.
     */
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

